import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ControllerTwoMenu {
    Stage primaryStage = new Stage();

    public void inputDataBaseItems() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("items.fxml"));

        primaryStage.setTitle("Предметы");
        primaryStage.setScene(new Scene(root, 759, 429));
        primaryStage.show();
    }
    public void inputDataBaseSpec() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("specialty.fxml"));

        primaryStage.setTitle("Специальности");
        primaryStage.setScene(new Scene(root, 759, 429));
        primaryStage.show();
    }
    public void inputDataBaseApplicants() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("applicants.fxml"));

        primaryStage.setTitle("Абитуриенты");
        primaryStage.setScene(new Scene(root, 759, 512));
        primaryStage.show();
    }
    public void inputDataBaseGroups() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("groups.fxml"));

        primaryStage.setTitle("Группы");
        primaryStage.setScene(new Scene(root, 759, 512));
        primaryStage.show();
    }
    public void inputDataBaseIntroductory() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("introductory.fxml"));

        primaryStage.setTitle("Вступительные");
        primaryStage.setScene(new Scene(root, 759, 512));
        primaryStage.show();
    }
}
