import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ControllerOneMenu {
    Stage primaryStage = new Stage();

    public void inputMenuTwo() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("menutwo.fxml"));

        primaryStage.setTitle("Приёмная комиссия");
        primaryStage.setScene(new Scene(root, 481, 281));
        primaryStage.show();
    }

    public void inputStat() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("statistic.fxml"));

        primaryStage.setTitle("Самые популярный специальности");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    public void inputNegativeExz() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("ExzNegative.fxml"));

        primaryStage.setTitle("Не прошедшие вступительные");
        primaryStage.setScene(new Scene(root, 610, 400));
        primaryStage.show();
    }
}
