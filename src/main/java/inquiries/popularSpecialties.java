package inquiries;

import database.BD;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pojo.PopulSpecTable;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class popularSpecialties {
    BD bd = new BD();

    private ObservableList<PopulSpecTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<PopulSpecTable> tablePop;

    @FXML
    private TableColumn<PopulSpecTable, String> spec;

    @FXML
    private TableColumn<PopulSpecTable, Integer> numApp;

    private ResultSet resultSet;
    private ArrayList nameSpec = new ArrayList();
    private ArrayList<Integer> numAp = new ArrayList<>();
    private ArrayList total = new ArrayList();

    public void displayStat() throws SQLException {
        setDataDB();
        setTableData();

        spec.setCellValueFactory(new PropertyValueFactory<PopulSpecTable, String>("nameSp"));
        numApp.setCellValueFactory(new PropertyValueFactory<PopulSpecTable, Integer>("numAp"));

        tablePop.setItems(table);

        for (int i = 0; i < total.size(); i += 2) {
            table.add(new PopulSpecTable(total.get(i).toString(), (int) total.get(i + 1)));
        }
    }

    private void setDataDB() throws SQLException {
        bd.connect();
        resultSet = bd.stat.executeQuery("SELECT * FROM `группы`");

        while (resultSet.next()) {
            nameSpec.add(resultSet.getString(3));
            numAp.add(resultSet.getInt(4));
        }
        bd.close();

    }

    private void setTableData() {
        for (int i = 0; i < nameSpec.size(); i++) {
            if (total.isEmpty()) {
                total.add(nameSpec.get(i));
                total.add(numAp.get(i));
            } else {
                boolean check = false;
                for (int j = 0; j < i; j++) {
                    if (nameSpec.get(i).equals(nameSpec.get(j))) {
                        total.set(j + 1, numAp.get(j) + numAp.get(i));
                        check = true;
                    }
                }
                if (!check) {
                    total.add(nameSpec.get(i));
                    total.add(numAp.get(i));
                }
            }
        }

    }

}
