package inquiries;

import database.BD;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pojo.ExmNegativeTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ExmNegative {
    BD bd = new BD();

    private ObservableList<ExmNegativeTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<ExmNegativeTable> tableExmNegative;

    @FXML
    private TableColumn<ExmNegativeTable, String> Fio;

    @FXML
    private TableColumn<ExmNegativeTable, Integer> jar;

    @FXML
    private TableColumn<ExmNegativeTable, Integer> codGr;

    private ArrayList<Integer> number = new ArrayList<>();
    private ArrayList<Integer> numberExm = new ArrayList<>();
    private ArrayList<Integer> numExm = new ArrayList<>();

    private void setDataBDIntrod() throws SQLException {
        bd.connect();
        ResultSet resultSet = bd.stat.executeQuery("SELECT * FROM `вступительные испытания`");
        while (resultSet.next()) {
            number.add(resultSet.getInt(4));
            numberExm.add(resultSet.getInt(1));
        }
        for (int i = 0; i < number.size(); i++) {
            if (number.get(i) < 3) {
                numExm.add(numberExm.get(i));
            }
        }
        bd.close();
    }

    private ArrayList<String> FIO = new ArrayList<>();
    private ArrayList<Integer> exmList = new ArrayList<>();
    private ArrayList<Integer> jars = new ArrayList<>();
    private ArrayList<Integer> codGrs = new ArrayList<>();

    private void setDataDBApplicants() throws SQLException{
        bd.connect();
        ResultSet resultSet1 = bd.stat.executeQuery("SELECT * FROM `абитуриенты`");
        while (resultSet1.next()) {
            FIO.add(resultSet1.getString(1));
            exmList.add(resultSet1.getInt(2));
            jars.add(resultSet1.getInt(3));
            codGrs.add(resultSet1.getInt(4));
        }

        bd.close();
    }

    public void displayExmNegative() throws SQLException {
        setDataBDIntrod();
        setDataDBApplicants();
        Fio.setCellValueFactory(new PropertyValueFactory<ExmNegativeTable, String>("name"));
        jar.setCellValueFactory(new PropertyValueFactory<ExmNegativeTable, Integer>("jar"));
        codGr.setCellValueFactory(new PropertyValueFactory<ExmNegativeTable, Integer>("codGr"));

        tableExmNegative.setItems(table);
        for (int i = 0; i < exmList.size(); i++) {
            for (int j = 0; j < numExm.size(); j++) {
                if (exmList.get(i).equals(numberExm.get(j))) {
                    table.add(new ExmNegativeTable(FIO.get(i), jars.get(i), codGrs.get(i)));
                }
            }

        }
    }


}
