import database.BD;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Controller {
    BD bd = new BD();
    Stage primaryStage = new Stage();

    @FXML
    TextField logUs;
    @FXML
    PasswordField pass;
    @FXML
    Label error;
    @FXML
    public void regUser() throws Exception {
        String logNewUser = logUs.getText();
        String passNewUser = pass.getText();
        if(bd.addUser(logNewUser, passNewUser)){
            inputMenu();
        }else {
            error.setText("Такой пользователь уже существует");
        }
    }
    @FXML
    public void  logUser() throws Exception {
        String logNewUser = logUs.getText();
        String passNewUser = pass.getText();
        if (bd.login(logNewUser, passNewUser)){
            inputMenu();
        }else {
            error.setText("Такого пользователя не существует");
        }
    }

    public void inputMenu() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));

        primaryStage.setTitle("Приёмная комиссия");
        primaryStage.setScene(new Scene(root, 200, 200));
        primaryStage.show();
    }
}
