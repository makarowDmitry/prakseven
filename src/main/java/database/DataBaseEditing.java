package database;


import java.sql.SQLException;
import java.util.ArrayList;

public class DataBaseEditing {
    BD bd = new BD();
    String sql;

    public void addLineTwoValues(String nameTable, ArrayList arrValues) throws SQLException {
        bd.connect();
        sql = "INSERT `" + nameTable + "` VALUES(" + arrValues.get(0) + ",'" + arrValues.get(1) + "')";
        bd.stat.executeUpdate(sql);
        bd.close();
    }

    public void addLineFourValues(String nameTable, ArrayList arrValues) throws SQLException {
        bd.connect();
        sql = "";
        switch (nameTable) {
            case "абитуриенты":
                sql = "INSERT `" + nameTable + "` VALUES('" + arrValues.get(0) + "'," + arrValues.get(1) + "," + arrValues.get(2) + "," + arrValues.get(3) + ")";
                break;
            case "группы":
                sql = "INSERT `" + nameTable + "` VALUES(" + arrValues.get(0) + "," + arrValues.get(1) + ",'" + arrValues.get(2) + "'," + arrValues.get(3) + ")";
                break;
            case "вступительные испытания":
                sql = "INSERT `" + nameTable + "` VALUES(" + arrValues.get(0) + "," + arrValues.get(1) + "," + arrValues.get(2) + "," + arrValues.get(3) + ")";
                break;
        }
        bd.stat.executeUpdate(sql);
        bd.close();
    }

    public void deleteLine(String tableName, String columnName, int valueUnique) throws SQLException {
        bd.connect();
        sql = "DELETE FROM `" + tableName + "` WHERE `" + columnName + "` = " + valueUnique;
        bd.stat.executeUpdate(sql);
        bd.close();
    }

    public void updateElementInt(String tableName, String columnName, int[] values) throws SQLException {
        bd.connect();
        sql = "UPDATE `" + tableName + "` SET `" + columnName + "` =" + values[0] + " WHERE " + tableName + "." + "`" + columnName + "` = " + values[1];
        bd.stat.executeUpdate(sql);
        bd.close();
    }

    public void updateElementString(String tableName, String columnName, String[] values) throws SQLException {
        bd.connect();
        sql = "UPDATE `" + tableName + "` SET `" + columnName + "` ='" + values[0] + "' WHERE " + tableName + "." + "`" + columnName + "` = '" + values[1] + "'";
        bd.stat.executeUpdate(sql);
        bd.close();
    }
}
