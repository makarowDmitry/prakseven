package database;

import java.sql.*;
import java.util.ArrayList;

public class BD {
    protected Connection conn;
    public Statement stat;
    private ResultSet resultSet;

    public void connect(){
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/приёмная+комиссия?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","mysql", "mysql");
            stat = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void close() throws SQLException {
        stat.close();
        conn.close();
    }


    public boolean addUser(String login, String password) throws SQLException {
        ArrayList<String> userLog = new ArrayList<>();
        ArrayList<String> userPassword = new ArrayList<>();
        boolean check = true;
        connect();
        resultSet = stat.executeQuery("SELECT * FROM пользователи");
        while (resultSet.next()){
            userLog.add(resultSet.getString(1));
            userPassword.add(resultSet.getString(2));
        }
        for (String s : userLog) {
            if (s.equals(login)) {
                check = false;
                break;
            }
        }
        String sql = "INSERT INTO пользователи (Логин, Пароль) VALUES ('" + login + "', '" + password + "');";
        if(check){
            stat.executeUpdate(sql);
        }
        close();
        return check;
    }

    public boolean login(String login, String password) throws SQLException {
        ArrayList<String> userLog = new ArrayList<>();
        ArrayList<String> userPassw = new ArrayList<>();
        boolean check = false;
        connect();
        resultSet = stat.executeQuery("SELECT * FROM пользователи");
        while (resultSet.next()){
            userLog.add(resultSet.getString(1));
            userPassw.add(resultSet.getString(2));
        }
        for (int i = 0; i < userLog.size() ; i++) {
            if (userLog.get(i).equals(login) && userPassw.get(i).equals(password)) {
                check = true;
                break;
            }
        }
        close();
        return check;
    }



}
