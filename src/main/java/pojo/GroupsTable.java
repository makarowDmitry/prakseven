package pojo;

public class GroupsTable {
    private int codGr;
    private int codSp;
    private String nameSp;
    private int numAb;

    public GroupsTable(int codGp,int codSp, String nameSp, int numAb){
        this.codGr = codGp;
        this.codSp = codSp;
        this.nameSp = nameSp;
        this.numAb = numAb;
    }

    public GroupsTable(){}



    public int getNumAb() {
        return numAb;
    }

    public void setNumAb(int numAb) {
        this.numAb = numAb;
    }

    public int getCodGr() {
        return codGr;
    }

    public void setCodGr(int codGr) {
        this.codGr = codGr;
    }

    public int getCodSp() {
        return codSp;
    }

    public void setCodSp(int codSp) {
        this.codSp = codSp;
    }

    public String getNameSp() {
        return nameSp;
    }

    public void setNameSp(String nameSp) {
        this.nameSp = nameSp;
    }
}
