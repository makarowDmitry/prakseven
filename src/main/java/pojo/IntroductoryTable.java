package pojo;

public class IntroductoryTable {
    private int numExz;
    private int codAp;
    private int codIt;
    private int rating;

    public  IntroductoryTable(int numExz, int codAp, int codIt, int rating){
        this.numExz = numExz;
        this.codAp = codAp;
        this.codIt = codIt;
        this.rating = rating;
    }

    public IntroductoryTable(){}

    public int getNumExz() {
        return numExz;
    }

    public void setNumExz(int numExz) {
        this.numExz = numExz;
    }

    public int getCodAp() {
        return codAp;
    }

    public void setCodAp(int codAp) {
        this.codAp = codAp;
    }

    public int getCodIt() {
        return codIt;
    }

    public void setCodIt(int codIt) {
        this.codIt = codIt;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
