package pojo;

public class SpecialtyTable {
    private int codSp;
    private String nameSp;

    public SpecialtyTable(int codSp, String nameSp){
        this.codSp = codSp;
        this.nameSp = nameSp;
    }

    public SpecialtyTable(){}

    public int getCodSp(){
        return codSp;
    }

    public void setCodSp(int codSp){
        this.codSp = codSp;
    }

    public  String getNameSp(){
        return nameSp;
    }

    public void setNameSp(String nameSp){
        this.nameSp = nameSp;
    }
}
