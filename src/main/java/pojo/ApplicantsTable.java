package pojo;

public class ApplicantsTable {
    private String name;
    private int numExz;
    private int jar;
    private int codGr;

    public ApplicantsTable(String name, int numExz, int jar, int codGr){
        this.name = name;
        this.numExz = numExz;
        this.jar = jar;
        this.codGr = codGr;
    }

    public ApplicantsTable(){}


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumExz() {
        return numExz;
    }

    public void setNumExz(int numExz) {
        this.numExz = numExz;
    }

    public int getJar() {
        return jar;
    }

    public void setJar(int jar) {
        this.jar = jar;
    }

    public int getCodGr() {
        return codGr;
    }

    public void setCodGr(int codGr) {
        this.codGr = codGr;
    }
}
