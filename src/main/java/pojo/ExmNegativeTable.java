package pojo;

public class ExmNegativeTable {
    private String name;
    private int jar;
    private int codGr;

    public ExmNegativeTable(String name, int jar, int codGr){
        this.name = name;
        this.jar = jar;
        this.codGr = codGr;
    }

    public ExmNegativeTable(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getJar() {
        return jar;
    }

    public void setJar(int jar) {
        this.jar = jar;
    }

    public int getCodGr() {
        return codGr;
    }

    public void setCodGr(int codGr) {
        this.codGr = codGr;
    }
}
