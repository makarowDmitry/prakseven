package pojo;

public class PopulSpecTable {
    private String nameSp;
    private int numAp;

    public PopulSpecTable(String nameSp, int numAp) {
        this.nameSp = nameSp;
        this.numAp = numAp;
    }

    public PopulSpecTable() {
    }

    public int getNumAp() {
        return numAp;
    }

    public void setNumAp(int numAp) {
        this.numAp = numAp;
    }

    public String getNameSp() {
        return nameSp;
    }

    public void setNameSp(String nameSp) {
        this.nameSp = nameSp;
    }
}
