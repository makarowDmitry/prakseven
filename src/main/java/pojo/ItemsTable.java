package pojo;

public class ItemsTable {
    private int codItem;
    private String nameItem;

    public ItemsTable(int codItem, String nameItem) {
        this.codItem = codItem;
        this.nameItem = nameItem;
    }

    public ItemsTable() {
    }

    public int getCodItem() {
        return codItem;
    }

    public void setCodItem(int codItem) {
        this.codItem = codItem;
    }

    public String getNameItem(){
        return nameItem;
    }

    public void setNameItem(String nameItem){
        this.nameItem = nameItem;
    }
}
