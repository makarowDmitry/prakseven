package controllersdatabase;

import database.BD;
import database.DataBaseEditing;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import pojo.ApplicantsTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Applicants {
    DataBaseEditing dataBaseEditing = new DataBaseEditing();
    @FXML
    TextField addValueOneAp;
    @FXML
    TextField addValueTwoAp;
    @FXML
    TextField addValueThreeAp;
    @FXML
    TextField addValueFourAp;

    public void addAp() throws SQLException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(addValueOneAp.getText());
        arrayList.add(Integer.parseInt(addValueTwoAp.getText()));
        arrayList.add(Integer.parseInt(addValueThreeAp.getText()));
        arrayList.add(Integer.parseInt(addValueFourAp.getText()));
        dataBaseEditing.addLineFourValues("абитуриенты", arrayList);
    }

    @FXML
    TextField deleteValueAp;

    public void deleteAp() throws SQLException {
        dataBaseEditing.deleteLine("абитуриенты", "Номер экзаменационного листа", Integer.parseInt(deleteValueAp.getText()));
    }

    @FXML
    TextField newValueOneAp;
    @FXML
    TextField newValueTwoAp;
    @FXML
    TextField newValueThreeAp;
    @FXML
    TextField newValueFourAp;
    @FXML
    TextField oldValueOneAp;
    @FXML
    TextField oldValueTwoAp;
    @FXML
    TextField oldValueThreeAp;
    @FXML
    TextField oldValueFourAp;

    public void updateAp() throws SQLException {
        if (!(newValueOneAp.getText().equals(""))) {
            String[] arrVal = {newValueOneAp.getText(), oldValueOneAp.getText()};
            dataBaseEditing.updateElementString("абитуриенты", "ФИО", arrVal);
        }
        if (!(newValueTwoAp.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueTwoAp.getText()), Integer.parseInt(oldValueTwoAp.getText())};
            dataBaseEditing.updateElementInt("абитуриенты", "Номер экзаменационного листа", arrVal);
        }
        if (!(newValueThreeAp.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueThreeAp.getText()), Integer.parseInt(oldValueThreeAp.getText())};
            dataBaseEditing.updateElementInt("абитуриенты", "Год рождения", arrVal);
        }
        if (!(newValueFourAp.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueFourAp.getText()), Integer.parseInt(oldValueFourAp.getText())};
            dataBaseEditing.updateElementInt("абитуриенты", "Код группы", arrVal);
        }

    }

    BD bd = new BD();

    private ObservableList<ApplicantsTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<ApplicantsTable> tableAp;

    @FXML
    private TableColumn<ApplicantsTable, String> name;

    @FXML
    private TableColumn<ApplicantsTable, Integer> numExz;

    @FXML
    private TableColumn<ApplicantsTable, Integer> jar;

    @FXML
    private TableColumn<ApplicantsTable, Integer> codGr;

    public void displayAp() throws SQLException {
        tableAp.getItems().clear();
        bd.connect();
        ResultSet resultSet = bd.stat.executeQuery("SELECT * FROM `абитуриенты`");

        ArrayList<String> arrCodGr = new ArrayList<>();
        ArrayList<Integer> arrCodSp = new ArrayList<>();
        ArrayList<Integer> arrNameSp = new ArrayList<>();
        ArrayList<Integer> arrNumAp = new ArrayList<>();
        while (resultSet.next()) {
            arrCodGr.add(resultSet.getString(1));
            arrCodSp.add(resultSet.getInt(2));
            arrNameSp.add(resultSet.getInt(3));
            arrNumAp.add(resultSet.getInt(4));
        }
        bd.close();

        name.setCellValueFactory(new PropertyValueFactory<ApplicantsTable, String>("name"));
        numExz.setCellValueFactory(new PropertyValueFactory<ApplicantsTable, Integer>("numExz"));
        jar.setCellValueFactory(new PropertyValueFactory<ApplicantsTable, Integer>("jar"));
        codGr.setCellValueFactory(new PropertyValueFactory<ApplicantsTable, Integer>("codGr"));

        tableAp.setItems(table);

        for (int i = 0; i < arrCodGr.size(); i++) {
            table.add(new ApplicantsTable(arrCodGr.get(i), arrCodSp.get(i),arrNameSp.get(i),arrNumAp.get(i)));
        }

    }

}
