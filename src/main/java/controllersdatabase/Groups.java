package controllersdatabase;

import database.BD;
import database.DataBaseEditing;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import pojo.GroupsTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Groups {
    DataBaseEditing dataBaseEditing = new DataBaseEditing();
    @FXML
    TextField addValueOneGr;
    @FXML
    TextField addValueTwoGr;
    @FXML
    TextField addValueThreeGr;
    @FXML
    TextField addValueFourGr;

    public void addGr() throws SQLException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.parseInt(addValueOneGr.getText()));
        arrayList.add(Integer.parseInt(addValueTwoGr.getText()));
        arrayList.add(addValueThreeGr.getText());
        arrayList.add(Integer.parseInt(addValueFourGr.getText()));
        dataBaseEditing.addLineFourValues("группы", arrayList);
    }

    @FXML
    TextField deleteValueGr;

    public void deleteGr() throws SQLException {
        dataBaseEditing.deleteLine("группы", "Код группы", Integer.parseInt(deleteValueGr.getText()));
    }

    @FXML
    TextField newValueOneGr;
    @FXML
    TextField newValueTwoGr;
    @FXML
    TextField newValueThreeGr;
    @FXML
    TextField newValueFourGr;
    @FXML
    TextField oldValueOneGr;
    @FXML
    TextField oldValueTwoGr;
    @FXML
    TextField oldValueThreeGr;
    @FXML
    TextField oldValueFourGr;

    public void updateGr() throws SQLException {
        if (!(newValueOneGr.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueOneGr.getText()), Integer.parseInt(oldValueOneGr.getText())};
            dataBaseEditing.updateElementInt("группы", "Код группы", arrVal);
        }
        if (!(newValueTwoGr.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueTwoGr.getText()), Integer.parseInt(oldValueTwoGr.getText())};
            dataBaseEditing.updateElementInt("группы", "Код специалности", arrVal);
        }
        if (!(newValueThreeGr.getText().equals(""))) {
            String[] arrVal = {newValueThreeGr.getText(), oldValueThreeGr.getText()};
            dataBaseEditing.updateElementString("группы", "Наименование специальности", arrVal);
        }
        if (!(newValueFourGr.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueFourGr.getText()), Integer.parseInt(oldValueFourGr.getText())};
            dataBaseEditing.updateElementInt("группы", "Общее число абитуриентов", arrVal);
        }

    }

    BD bd = new BD();

    private ObservableList<GroupsTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<GroupsTable> tableGr;

    @FXML
    private TableColumn<GroupsTable, Integer> codGr;

    @FXML
    private TableColumn<GroupsTable, Integer> codSp;

    @FXML
    private TableColumn<GroupsTable, String> nameSp;

    @FXML
    private TableColumn<GroupsTable, Integer> numAp;

    public void displayGr() throws SQLException {
        tableGr.getItems().clear();
        bd.connect();
        ResultSet resultSet = bd.stat.executeQuery("SELECT * FROM `группы`");

        ArrayList<Integer> arrCodGr = new ArrayList<>();
        ArrayList<Integer> arrCodSp = new ArrayList<>();
        ArrayList<String> arrNameSp = new ArrayList<>();
        ArrayList<Integer> arrNumAp = new ArrayList<>();
        while (resultSet.next()) {
            arrCodGr.add(resultSet.getInt(1));
            arrCodSp.add(resultSet.getInt(2));
            arrNameSp.add(resultSet.getString(3));
            arrNumAp.add(resultSet.getInt(4));
        }
        bd.close();

        codGr.setCellValueFactory(new PropertyValueFactory<GroupsTable, Integer>("codGp"));
        codSp.setCellValueFactory(new PropertyValueFactory<GroupsTable, Integer>("codSp"));
        nameSp.setCellValueFactory(new PropertyValueFactory<GroupsTable, String>("nameSp"));
        numAp.setCellValueFactory(new PropertyValueFactory<GroupsTable, Integer>("numAp"));

        tableGr.setItems(table);

        for (int i = 0; i < arrCodGr.size(); i++) {
            table.add(new GroupsTable(arrCodGr.get(i), arrCodSp.get(i),arrNameSp.get(i),arrNumAp.get(i)));
        }

    }

}
