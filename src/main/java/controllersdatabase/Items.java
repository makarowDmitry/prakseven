package controllersdatabase;

import database.BD;
import database.DataBaseEditing;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import pojo.ItemsTable;
import pojo.SpecialtyTable;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Items {
    DataBaseEditing dataBaseEditing = new DataBaseEditing();
    @FXML
    TextField addValueOne;
    @FXML
    TextField addValueTwo;

    public void addItem() throws SQLException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.parseInt(addValueOne.getText()));
        arrayList.add(addValueTwo.getText());
        dataBaseEditing.addLineTwoValues("предметы", arrayList);
    }

    @FXML
    TextField deleteValue;

    public void deleteItem() throws SQLException {
        dataBaseEditing.deleteLine("предметы", "Код предмета", Integer.parseInt(deleteValue.getText()));
    }

    @FXML
    TextField newValueOne;
    @FXML
    TextField newValueTwo;
    @FXML
    TextField oldValueOne;
    @FXML
    TextField oldValueTwo;

    public void updateItem() throws SQLException {
        if (!(newValueOne.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueOne.getText()), Integer.parseInt(oldValueOne.getText())};
            dataBaseEditing.updateElementInt("предметы", "Код предмета", arrVal);
        }
        if (!(newValueTwo.getText().equals(""))) {
            String[] arrVal = {newValueTwo.getText(), oldValueTwo.getText()};
            dataBaseEditing.updateElementString("предметы", "Название предмета", arrVal);
        }
    }

    private BD bd = new BD();

    private ObservableList<ItemsTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<ItemsTable> tableItems;

    @FXML
    private TableColumn<ItemsTable, Integer> columnCodItems;

    @FXML
    private TableColumn<ItemsTable, String> columnNameItems;

    public void displayItem() throws SQLException {
        tableItems.getItems().clear();
        bd.connect();
        ResultSet resultSet = bd.stat.executeQuery("SELECT * FROM специальности");

        ArrayList<Integer> arrCod = new ArrayList<>();
        ArrayList<String> arrName = new ArrayList<>();
        while (resultSet.next()) {
            arrCod.add(resultSet.getInt(1));
            arrName.add(resultSet.getString(2));
        }
        bd.close();

        columnCodItems.setCellValueFactory(new PropertyValueFactory<ItemsTable, Integer>("codItem"));
        columnNameItems.setCellValueFactory(new PropertyValueFactory<ItemsTable, String>("nameItem"));

        tableItems.setItems(table);

        for (int i = 0; i < arrCod.size(); i++) {
            table.add(new ItemsTable(arrCod.get(i), arrName.get(i)));
        }

    }
}
