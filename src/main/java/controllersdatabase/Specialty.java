package controllersdatabase;

import database.BD;
import database.DataBaseEditing;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import pojo.ItemsTable;
import pojo.SpecialtyTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Specialty {
    DataBaseEditing dataBaseEditing = new DataBaseEditing();
    @FXML
    TextField addValueOneSp;
    @FXML
    TextField addValueTwoSp;

    public void addSpec() throws SQLException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.parseInt(addValueOneSp.getText()));
        arrayList.add(addValueTwoSp.getText());
        dataBaseEditing.addLineTwoValues("специальности", arrayList);
    }

    @FXML
    TextField deleteValueSp;

    public void deleteSpec() throws SQLException {
        dataBaseEditing.deleteLine("специальности", "Код специальности", Integer.parseInt(deleteValueSp.getText()));
    }

    @FXML
    TextField newValueOneSp;
    @FXML
    TextField newValueTwoSp;
    @FXML
    TextField oldValueOneSp;
    @FXML
    TextField oldValueTwoSp;

    public void updateSpec() throws SQLException {
        if (!(newValueOneSp.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueOneSp.getText()), Integer.parseInt(oldValueOneSp.getText())};
            dataBaseEditing.updateElementInt("специальности", "Код специальности", arrVal);
        }
        if (!(newValueTwoSp.getText().equals(""))) {
            String[] arrVal = {newValueTwoSp.getText(), oldValueTwoSp.getText()};
            dataBaseEditing.updateElementString("специальности", "Наименование специальности", arrVal);
        }
    }

    private BD bd = new BD();

    private ObservableList<SpecialtyTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<SpecialtyTable> tableSp;

    @FXML
    private TableColumn<SpecialtyTable, Integer> columnCodSp;

    @FXML
    private TableColumn<SpecialtyTable, String> columnNameSp;

    public void displaySp() throws SQLException {
        tableSp.getItems().clear();
        bd.connect();
        ResultSet resultSet = bd.stat.executeQuery("SELECT * FROM специальности");

        ArrayList<Integer> arrCod = new ArrayList<>();
        ArrayList<String> arrName = new ArrayList<>();
        while (resultSet.next()) {
            arrCod.add(resultSet.getInt(1));
            arrName.add(resultSet.getString(2));
        }
        bd.close();

        columnCodSp.setCellValueFactory(new PropertyValueFactory<SpecialtyTable, Integer>("codSp"));
        columnNameSp.setCellValueFactory(new PropertyValueFactory<SpecialtyTable, String>("nameSp"));

        tableSp.setItems(table);

        for (int i = 0; i < arrCod.size(); i++) {
            table.add(new SpecialtyTable(arrCod.get(i), arrName.get(i)));
        }

    }
}
