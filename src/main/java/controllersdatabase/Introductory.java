package controllersdatabase;

import database.BD;
import database.DataBaseEditing;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import pojo.IntroductoryTable;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Introductory {
    DataBaseEditing dataBaseEditing = new DataBaseEditing();
    @FXML
    TextField addValueOneIntr;
    @FXML
    TextField addValueTwoIntr;
    @FXML
    TextField addValueThreeIntr;
    @FXML
    TextField addValueFourIntr;

    public void addIntr() throws SQLException {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.parseInt(addValueOneIntr.getText()));
        arrayList.add(Integer.parseInt(addValueTwoIntr.getText()));
        arrayList.add(Integer.parseInt(addValueThreeIntr.getText()));
        arrayList.add(Integer.parseInt(addValueFourIntr.getText()));
        dataBaseEditing.addLineFourValues("вступительные испытания", arrayList);
    }

    @FXML
    TextField deleteValueIntr;

    public void deleteIntr() throws SQLException {
        dataBaseEditing.deleteLine("вступительные испытания", "Номер экзаменационного листа", Integer.parseInt(deleteValueIntr.getText()));
    }

    @FXML
    TextField newValueOneIntr;
    @FXML
    TextField newValueTwoIntr;
    @FXML
    TextField newValueThreeIntr;
    @FXML
    TextField newValueFourIntr;
    @FXML
    TextField oldValueOneIntr;
    @FXML
    TextField oldValueTwoIntr;
    @FXML
    TextField oldValueThreeIntr;
    @FXML
    TextField oldValueFourIntr;

    public void updateIntr() throws SQLException {
        if (!(newValueOneIntr.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueOneIntr.getText()), Integer.parseInt(oldValueOneIntr.getText())};
            dataBaseEditing.updateElementInt("вступительные испытания", "Номер экзаменационного листа", arrVal);
        }
        if (!(newValueTwoIntr.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueTwoIntr.getText()), Integer.parseInt(oldValueTwoIntr.getText())};
            dataBaseEditing.updateElementInt("вступительные испытания", "Код абитуриента", arrVal);
        }
        if (!(newValueThreeIntr.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueThreeIntr.getText()), Integer.parseInt(oldValueThreeIntr.getText())};
            dataBaseEditing.updateElementInt("вступительные испытания", "Код предмета", arrVal);
        }
        if (!(newValueFourIntr.getText().equals(""))) {
            int[] arrVal = {Integer.parseInt(newValueFourIntr.getText()), Integer.parseInt(oldValueFourIntr.getText())};
            dataBaseEditing.updateElementInt("вступительные испытания", "Полученная оценка", arrVal);
        }

    }

    BD bd = new BD();

    private ObservableList<IntroductoryTable> table = FXCollections.observableArrayList();

    @FXML
    private TableView<IntroductoryTable> tableIntr;

    @FXML
    private TableColumn<IntroductoryTable, Integer> columnNumExz;

    @FXML
    private TableColumn<IntroductoryTable, Integer> columnCodAp;

    @FXML
    private TableColumn<IntroductoryTable, Integer> columnCodItem;

    @FXML
    private TableColumn<IntroductoryTable, Integer> columnRating;

    public void displayItem() throws SQLException {
        tableIntr.getItems().clear();
        bd.connect();
        ResultSet resultSet = bd.stat.executeQuery("SELECT * FROM `вступительные испытания`");

        ArrayList<Integer> arrNumExz = new ArrayList<>();
        ArrayList<Integer> arrCodAp = new ArrayList<>();
        ArrayList<Integer> arrCodItem = new ArrayList<>();
        ArrayList<Integer> arrRating = new ArrayList<>();
        while (resultSet.next()) {
            arrNumExz.add(resultSet.getInt(1));
            arrCodAp.add(resultSet.getInt(2));
            arrCodItem.add(resultSet.getInt(3));
            arrRating.add(resultSet.getInt(4));
        }
        bd.close();

        columnNumExz.setCellValueFactory(new PropertyValueFactory<IntroductoryTable, Integer>("numExz"));
        columnCodAp.setCellValueFactory(new PropertyValueFactory<IntroductoryTable, Integer>("codAp"));
        columnCodItem.setCellValueFactory(new PropertyValueFactory<IntroductoryTable, Integer>("codIt"));
        columnRating.setCellValueFactory(new PropertyValueFactory<IntroductoryTable, Integer>("rating"));

        tableIntr.setItems(table);

        for (int i = 0; i < arrNumExz.size(); i++) {
            table.add(new IntroductoryTable(arrNumExz.get(i), arrCodAp.get(i),arrCodItem.get(i),arrRating.get(i)));
        }

    }

}
