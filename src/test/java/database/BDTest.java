package database;

import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class BDTest {
    BD bd = new BD();

    @Test
    public void addUserFalse() throws SQLException {
        assertFalse(bd.addUser("admin","123"));
    }
    @Test
    public void loginTrue() throws SQLException {
        assertTrue(bd.login("admin","123"));
    }
    @Test
    public void loginFalse() throws SQLException {
        assertFalse(bd.login("admin","12"));
    }


}